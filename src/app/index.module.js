(function() {
  'use strict';

  angular
    .module('cswFront', [
                         'ngAnimate',
                         'ngCookies',
                         'ngTouch',
                         'ngSanitize',
                         'ngMessages',
                         'ngAria',
                         'ngResource',
                         'ui.router',
                         'ui.bootstrap',
                         'toastr',
                         'satellizer',
                         'ngMdIcons'
                        ]);

})();

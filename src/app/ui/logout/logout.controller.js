(function() {
  'use strict';

  angular
    .module('cswFront')
    .controller('LogoutController', LogoutController);

  /** @ngInject */
  function LogoutController($location, $auth, toastr) {
    if (!$auth.isAuthenticated()) { return; }
    $auth.logout()
      .then(function() {
        toastr.info('Você foi deslogado do sistema.');
        $location.path('/login');
      });
  }
})();

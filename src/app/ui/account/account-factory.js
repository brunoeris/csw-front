angular
  .module('cswFront')
  .factory('Account', Account);

  function Account($http, URL_BASE_API) {

        var o = {
          getProfile: function() {
            return $http.get(URL_BASE_API + '/api/v1/users/me');
          }
        };
        return o;
  }

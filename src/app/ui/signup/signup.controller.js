(function () {
  'use strict';

  angular
    .module('cswFront')
    .controller('SignupController', SignupController);

  /** @ngInject */
  function SignupController($scope, $http, $location, $auth, toastr, $log, URL_BASE_API) {
    var vm = this;

    vm.submit = function () {
        $http.post(
                    URL_BASE_API+'/api/v1/users',
                    {
                      user: {
                              full_name: vm.user.full_name,
                              email: vm.user.email,
                              password: $scope.pw
                      }
                    })
            .then(function (resp) {
              $location.path('/verify');
              toastr.success('Cadastro efetuado com sucesso.');
        }, function (error) {
            var errors = [];
            angular.forEach(error.data, function (error, index) {
              errors.push(' ' + index + ': ' + error);
            });
            toastr.error('<p>' + errors + '</p>', 'Erro ao criar usuário.', {
              progressBar: false,
              timeOut: 9000,
              closeButton: true
            });
          });
    };

  }
})();

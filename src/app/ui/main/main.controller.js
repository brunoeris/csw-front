(function() {
  'use strict';

  angular
    .module('cswFront')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($location) {
    var vm = this;

    vm.done = function () {
      $('#myModal').modal('hide');
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
      $location.path("/done");
    }
  }
})();

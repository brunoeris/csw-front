(function() {
  'use strict';

  angular
    .module('cswFront')
    .constant('URL_BASE_API', 'https://csw-server.herokuapp.com')
    // .constant('URL_BASE_API', 'http://localhost:3000')
})();

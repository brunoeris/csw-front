(function() {
  'use strict';

  angular
    .module('cswFront')
    .directive('checkStrength', checkStrength);

  /** @ngInject */
  function checkStrength() {
    var directive = {
      replace: false,
      restrict: 'EACM',
      link: function ($scope, iElement, iAttrs) {
          console.log($scope);
          console.log(iElement);
          console.log(iAttrs);
          var strength = {
              colors: ['#DDDDDD', '#F00', '#FF0', '#1fbd06', '#04dede'],
              mesureStrength: function (p) {

                  var _force = 0;
                  var _regex = /[$-/:-?{-~!"^_`\[\]]/g;

                  var _lowerLetters = /[a-z]+/.test(p);
                  var _upperLetters = /[A-Z]+/.test(p);
                  var _numbers = /[0-9]+/.test(p);
                  var _symbols = _regex.test(p);

                  var _flags = [_lowerLetters, _upperLetters, _numbers, _symbols];
                  var _passedMatches = $.grep(_flags, function (el) { return el === true; }).length;

                  _force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
                  _force += _passedMatches * 10;

                  // penality (short password)
                  _force = (p.length <= 4) ? Math.min(_force, 10) : _force;

                  // penality (poor variety of characters)
                  _force = (_passedMatches == 1) ? Math.min(_force, 10) : _force;
                  _force = (_passedMatches == 2) ? Math.min(_force, 20) : _force;
                  _force = (_passedMatches == 3) ? Math.min(_force, 40) : _force;

                  return _force;

              },
              getColor: function (s) {

                  var idx = 0;
                  if (s <= 10) { idx = 0; }
                  else if (s <= 20) { idx = 1; }
                  else if (s <= 30) { idx = 2; }
                  else if (s <= 40) { idx = 3; }
                  else { idx = 4; }

                  return { idx: idx + 1, col: this.colors[idx] };

              }
          };

          $scope.$watch(iAttrs.checkStrength, function () {
              if ($scope.pw === undefined) {
                  iElement.css({ "display": "none"  });
              } else {
                  var c = strength.getColor(strength.mesureStrength($scope.pw));

                  iElement.css({ "display": "inline" });
                  iElement.children('li')
                      .css({ "background": "#DDD" })
                      .slice(0, c.idx)
                      .css({ "background": c.col });
                  if (c.idx) {
                    switch(c.idx) {
                        case 1:
                            iElement.children('b')
                                    .html('Utilize maiúsculas, minúsculas, números e símbolos para obter uma senha segura')
                                    .css({ "color": c.col });
                            break;
                        case 2:
                            iElement.children('b')
                                    .html('A senha deve conter pelo menos 8 caracteres')
                                    .css({ "color": c.col });
                            break;
                        case 3:
                            iElement.children('b')
                                    .html('Quase lá!')
                                    .css({ "color": c.col });
                            break;
                        case 4:
                            iElement.children('b')
                                    .html('Ótima senha')
                                    .css({ "color": c.col });
                            break;
                        case 5:
                            iElement.children('b')
                                    .html('Incrível o/')
                                    .css({ "color": c.col });
                            break;
                        default:
                            console.log('Default');
                    }
                  }
              }
          });

      },
      template: '<li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li><br><b class="pwbold"></b>'
    };

    return directive;
  }

})();

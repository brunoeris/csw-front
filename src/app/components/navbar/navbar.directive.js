(function() {
  'use strict';

  angular
    .module('cswFront')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
          creationDate: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController($scope, $auth) {
      var vm = this;

      vm.isAuthenticated = function () {
        return $auth.isAuthenticated();
      };
    }
  }

})();
